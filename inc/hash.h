/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#ifndef HASH_H
#define HASH_H

////////////////////////////////////////////////////////////////////////////////
/// INCLUDES
////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

////////////////////////////////////////////////////////////////////////////////
/// DEFINES
////////////////////////////////////////////////////////////////////////////////

#define		HT_MIN_SIZE			32
#define		HT_THRESHOLD_UP		((long double)1.0)
#define		HT_THRESHOLD_DOWN	((long double)0.5)

////////////////////////////////////////////////////////////////////////////////
/// TYPEDEFS
////////////////////////////////////////////////////////////////////////////////

typedef	unsigned int		uint;
typedef	unsigned long long	ullint;
typedef	long long			llint;
typedef	long double			ldouble;

typedef	struct s_ht_node	t_ht_node;
typedef	struct s_ht			t_ht;

////////////////////////////////////////////////////////////////////////////////
/// STRUCTURES
////////////////////////////////////////////////////////////////////////////////

struct			s_ht_node {
	t_ht_node	*next;
	void		*value;
	char		key[];
};

struct			s_ht {
	t_ht_node	**nodes;
	void		(*free)(void*);
	size_t		size;
	size_t		num_elem;
};

////////////////////////////////////////////////////////////////////////////////
/// FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

/// CREATE / DELETE
t_ht			*ht_create(t_ht *table, void (*func)(void*));
void			ht_clear(t_ht *table);
void			ht_delete(t_ht **table);
/// SET / GET / DEL / MOVE
int				ht_set(t_ht *table, char *key, void *value);
void			*ht_get(t_ht *table, char *key);
void			ht_del(t_ht *table, char *key);
void			ht_move(t_ht *table_from, t_ht *table_to, char *key);
/// TOOLS
void			ht_print(t_ht *table, void (*func)(void *));
char			**ht_retrieve_keys(t_ht *table);
void			**ht_retrieve_values(t_ht *table);
void			ht_iter(t_ht *table, void *(*func)(char *, void *));

/// LIBRARY INTERNAL FUNCTIONS (DO NO USE)
size_t			ht__compute_hash(t_ht *table, char *str);
void			ht__scale_table(t_ht *table);
void			ht__add_node(t_ht *table, t_ht_node *node, size_t hash);

#endif
