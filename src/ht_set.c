/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "hash.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static t_ht_node	*create_node(char *key, void *value) {
	t_ht_node		*node;
	size_t			i;

	node = malloc(sizeof(t_ht_node) + sizeof(char) * (strlen(key) + 1));
	if (node == NULL)
		return NULL;
	node->next = NULL;
	node->value = value;
	i = 0;
	while (key[i] != 0) {
		node->key[i] = key[i];
		++i;
	}
	return node;
}

static int			add_node(t_ht *table, size_t hash, char *key, void *value) {
	t_ht_node		*new_node;
	t_ht_node		*node;

	node = table->nodes[hash];
	/// UPDATE EXISTANT NODE
	while (node != NULL) {
		if (strcmp(&(node->key[0]), key) == 0) {
			if (table->free != NULL)
				table->free(node->value);
			node->value = value;
			return 1;
		}
		node = node->next;
	}
	/// CREATE NEW NODE
	new_node = create_node(key, value);
	if (new_node == NULL)
		return 0;
	/// INSERT NEW NODE
	ht__add_node(table, new_node, hash);
	/// RESCALE TABLE
	ht__scale_table(table);
	return 1;
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

int			ht_set(t_ht *table, char *key, void *value) {
	size_t	hash;

	if (table == NULL || key == NULL)
		return 0;
	hash = ht__compute_hash(table, key);
	if (add_node(table, hash, key, value) == 0)
		return 0;
	return 1;
}
