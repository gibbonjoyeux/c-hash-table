/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "hash.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static void		scale_down(
	t_ht*		table) {
	t_ht_node*	node;
	t_ht_node*	next;
	t_ht_node**	nodes;
	t_ht_node**	prev_nodes;
	size_t		prev_size;
	size_t		i;

	/// [1] ALLOC NODE ARRAY
	nodes = (t_ht_node**)calloc(table->size / 2, sizeof(t_ht_node *));
	if (nodes == NULL)
		return;
	/// [2] TRANSFER NODES
	prev_nodes = table->nodes;
	prev_size = table->size;
	table->nodes = nodes;
	table->size /= 2;
	table->num_elem = 0;
	i = 0;
	while (i < prev_size) {
		node = prev_nodes[i];
		while (node != NULL) {
			ht__add_node(table, node, ht__compute_hash(table, &(node->key[0])));
			next = node->next;
			node->next = NULL;
			node = next;
		}
		++i;
	}
}

static void		scale_up(
	t_ht*		table) {
	t_ht_node*	node;
	t_ht_node*	next;
	t_ht_node**	nodes;
	t_ht_node**	prev_nodes;
	size_t		prev_size;
	size_t		i;

	/// [1] ALLOC NODE ARRAY
	nodes = (t_ht_node**)calloc(table->size * 2, sizeof(t_ht_node**));
	if (nodes == NULL)
		return;
	prev_nodes = table->nodes;
	prev_size = table->size;
	table->nodes = nodes;
	table->size *= 2;
	table->num_elem = 0;
	/// [2] TRANSFER NODES
	i = 0;
	while (i < prev_size) {
		node = prev_nodes[i];
		while (node != NULL) {
			ht__add_node(table, node, ht__compute_hash(table, &(node->key[0])));
			next = node->next;
			node->next = NULL;
			node = next;
		}
		++i;
	}
	free(prev_nodes);
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void		ht__scale_table(
	t_ht*		table) {
	long double	threshold;

	/// MINIMUM SIZE
	if (table->size <= HT_MIN_SIZE)
		return;
	/// THRESHOLD DOWN
	threshold = (long double)table->size * HT_THRESHOLD_DOWN;
	if (table->num_elem < threshold) {
		scale_down(table);
		return;
	}
	/// THRESHOLD UP
	threshold = (long double)table->size * HT_THRESHOLD_UP;
	if (table->num_elem + 1 > threshold) {
		scale_up(table);
		return;
	}
}
