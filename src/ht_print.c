/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "hash.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static int		compute_column_width(t_ht *table) {
	t_ht_node	*node;
	int			max;
	int			width;
	int			i;

	max = 0;
	i = 0;
	while ((size_t)i < table->size) {
		node = table->nodes[i];
		while (node != NULL) {
			width = strlen(node->key);
			if (width > max)
				max = width;
			node = node->next;
		}
		++i;
	}
	return max;
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void			ht_print(t_ht *table, void (*func)(void *)) {
	t_ht_node	*node;
	size_t		collisions;
	size_t		i;
	int			column_width;

	if (table == NULL)
		return;
	printf("table size: %zu\ntable elements: %zu\n", table->size, table->num_elem);
	column_width = compute_column_width(table);
	collisions = 0;
	i = 0;
	while (i < table->size) {
		node = table->nodes[i];
		while (node != NULL) {
			printf("%-*s : ", column_width, node->key);
			func(node->value);
			node = node->next;
			if (node != NULL)
				++collisions;
		}
		++i;
	}
	printf("collisions: %zu\n", collisions);
}
