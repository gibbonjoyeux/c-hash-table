/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "hash.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

t_ht		*ht_create(t_ht *table, void (*free)(void*)) {
	char	allocated;

	/// [1] ALLOCATE TABLE
	allocated = 0;
	if (table == NULL) {
		table = (t_ht *)malloc(sizeof(t_ht));
		if (table == NULL)
			return NULL;
		allocated = 1;
	}
	/// [2] ALLOCATE NODES
	table->size = HT_MIN_SIZE;
	table->num_elem = 0;
	table->nodes = (t_ht_node**)calloc(HT_MIN_SIZE, sizeof(t_ht_node*));
	if (table->nodes == NULL) {
		if (allocated == 1)
			free(table);
		return NULL;
	}
	/// [3] INIT FREE FUNCTION
	table->free = free;
	return table;
}
