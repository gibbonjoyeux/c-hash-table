/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "hash.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void			ht_del(t_ht *table, char *key) {
	t_ht_node	*node;
	t_ht_node	*prev;
	size_t		hash;

	if (table == NULL || key == NULL)
		return;
	/// [1] COMPUTE HASH
	hash = ht__compute_hash(table, key);
	node = table->nodes[hash];
	/// [2] REMOVE ELEMENT
	prev = NULL;
	while (node != NULL) {
		if (strcmp(&(node->key[0]), key) == 0) {
			/// REMOVE NODE FROM TABLE
			if (prev != NULL)
				prev->next = node->next;
			else
				table->nodes[hash] = node->next;
			free(node);
			/// RESCALE TABLE
			table->num_elem -= 1;
			ht__scale_table(table);
			return;
		}
		prev = node;
		node = node->next;
	}
}
