/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "hash.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void			*ht_get(t_ht *table, char *key) {
	t_ht_node	*node;
	size_t		hash;

	/// [1] COMPUTE HASH
	hash = ht__compute_hash(table, key);
	node = table->nodes[hash];
	/// [2] RETRIEVE ELEMENT
	while (node != NULL) {
		if (strcmp(&(node->key[0]), key) == 0)
			return node->value;
		node = node->next;
	}
	return NULL;
}

//void			*ht_get(t_ht *table, char *key) {
//	t_ht_node	*node;
//	size_t		i;
//
//	i = 0;
//	while (i < table->size) {
//		node = table->nodes[i];
//		while (node != NULL) {
//			if (strcmp(key, &(node->key[0])) == 0) {
//				return node->value;
//			}
//			node = node->next;
//		}
//		++i;
//	}
//	return NULL;
//}
