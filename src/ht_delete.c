/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "hash.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void			ht_delete(t_ht **table_addr) {
	t_ht		*table;

	if (table_addr == NULL || *table_addr == NULL)
		return;
	table = *table_addr;
	/// [1] FREE TABLE CONTENT
	ht_clear(table);
	/// [2] FREE TABLE
	free(table);
	*table_addr = NULL;
}
