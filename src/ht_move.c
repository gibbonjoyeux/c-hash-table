/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "hash.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static void		scale_table(t_ht *table) {
	long double	threshold;
	t_ht_node	*node;
	t_ht_node	*next;
	t_ht_node	**nodes;
	t_ht_node	**prev_nodes;
	size_t		prev_size;
	size_t		i;

	/// MINIMUM SIZE
	if (table->size <= HT_MIN_SIZE)
		return;
	/// UNDER THRESHOLD
	threshold = (long double)table->size * HT_THRESHOLD_DOWN;
	if (table->num_elem >= threshold)
		return;
	nodes = (t_ht_node **)calloc(table->size / 2, sizeof(t_ht_node *));
	if (nodes == NULL)
		return;
	/// TRANSFER NODES
	prev_nodes = table->nodes;
	prev_size = table->size;
	table->nodes = nodes;
	table->size /= 2;
	table->num_elem = 0;
	i = 0;
	while (i < prev_size) {
		node = prev_nodes[i];
		while (node != NULL) {
			ht__add_node(table, node, ht__compute_hash(table, node->key));
			next = node->next;
			node->next = NULL;
			node = next;
		}
		++i;
	}
}

static int		add_node(
	t_ht		*table,
	size_t		hash,
	char		*key,
	t_ht_node	*node_new) {
	t_ht_node	*node;

	node = table->nodes[hash];
	/// REPLACE EXISTANT NODE
	while (node != NULL) {
		if (strcmp(&(node->key[0]), key) == 0) {
			if (table->free != NULL)
				table->free(node->value);
			node->value = node_new->value;
			free(node_new);
			return 1;
		}
		node = node->next;
	}
	/// INSERT NODE
	ht__add_node(table, node_new, hash);
	return 1;
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void			ht_move(t_ht *table_from, t_ht *table_to, char *key) {
	t_ht_node	*node;
	t_ht_node	*prev;
	size_t		hash;

	if (table_from == NULL || table_to == NULL || key == NULL)
		return;
	/// [1] COMPUTE HASH
	hash = ht__compute_hash(table_from, key);
	node = table_from->nodes[hash];
	/// [2] REMOVE ELEMENT
	prev = NULL;
	while (node != NULL) {
		if (strcmp(&(node->key[0]), key) == 0) {
			/// REMOVE ITEM FROM TABLE
			if (prev != NULL)
				prev->next = node->next;
			else
				table_from->nodes[hash] = node->next;
			/// RESIZE TABLE
			table_from->num_elem -= 1;
			scale_table(table_from);
			/// INSERT ITEM TO TABLE
			add_node(table_to, hash, key, node);
			return;
		}
		prev = node;
		node = node->next;
	}
}
