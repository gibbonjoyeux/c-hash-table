/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hash.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void		*iter(char *key, void *value) {
	((char *)value)[7] = '!';
	return value;
}

int			main(void) {
	t_ht	*table;
	int		i;
	char	*str;
	char	**values;
	char	**keys;

	table = ht_create();
	printf("%p\n", table);

	for (i = 0; i < 10; ++i) {
		str = strdup("coucou  ");
		str[7] = 'a' + i;
		ht_set(table, str, (void *)str);
	}
	ht_print(table, &puts);

	ht_iter(table, &iter);
	ht_print(table, &puts);

	values = ht_retrieve_keys(table);
	for (i = 0; i < table->num_elem; ++i)
		puts(values[i]);
	free(values);
	values = ht_retrieve_values(table);
	for (i = 0; i < table->num_elem; ++i)
		puts(values[i]);
	free(values);

	ht_delete(&table, free);
	printf("%p\n", table);

	return 0;
}
